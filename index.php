<?php
require "connect.inc.php";

$advertisements = "";
$query_run = mysqli_query($con, "SELECT * FROM tb_advertisement");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $ads_title = $row['ads_title'];
        $ads_image=$row['ads_image'];
        $price_range = $row['price_range'];


        $advertisements .= '<div class="col-md-4  m-b-20">
	                            <div class="text-center" style="background-size: cover;background-repeat: no-repeat;background-image:url('. $ads_image.');height:400px;width:100%">
	
                                <div class="ads-label ">
                                    <div class="title">' . $ads_title . '</div>
                                    <div class="price">FROM<span>$' . $price_range . '</span></div>
                                    <a class="btn btn-danger" href="details.php?ads_id='.$ads_id.'">LEARN MORE</a>
                                </div>
	</div>
</div>';

// echo $advertisements;
    }
}
?>
<html lang="en">

<?php include('header.php'); ?>

<body style="background: #f1f1f1">

<?php include('navigation.php'); ?>

<div class="content">
    <main role="main">

        <!-- Main jumbotron for a primary marketing message or call to action -->

            <div class="jumbotron" style="padding:0">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="assets/images/slide.jpg" alt="Chania">
                            <div class="carousel-caption">
                                <h3>Annapurna</h3>
                                <p>LA is always so much fun!</p>
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/slide1.jpg" alt="Chicago">
                            <div class="carousel-caption">
                                <h3>rara lake</h3>
                                <p>Thank you, Chicago!</p>
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/slide2.jpg" alt="New York">
                            <div class="carousel-caption">
                                <h3>River side</h3>
                                <p>We love the Big Apple!</p>
                            </div>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>


            <div class="container" >
                <!-- Example row of columns -->
                <div class="row">

                    <?php echo $advertisements; ?>
                </div>

                <hr>

            </div> <!-- /container -->



    </main>

    <?php include('footer.php'); ?>

</div>


<!-- Bootstrap core JavaScript

================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../../../assets/js/vendor/popper.min.js"></script>
<script src="../../../../dist/js/bootstrap.min.js"></script> -->
</body>
</html>

