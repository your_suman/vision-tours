<?php
require "connect.inc.php";

$advertisements = "";
$searchParameter=$_POST['search'];
$query_run = mysqli_query($con, "SELECT * FROM tb_advertisement WHERE `ads_title` LIKE '%".$searchParameter."%'");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $ads_title = $row['ads_title'];
        $price_range = $row['price_range'];


        $advertisements .= '<div class="col-md-4  m-b-20">
	                            <div class="text-center" style="background-size: cover;background-repeat: no-repeat;background-image:url(assets/images/ban_img2.jpg);height:400px;width:100%">
	
                                <div class="ads-label ">
                                    <div class="title">' . $ads_title . '</div>
                                    <div class="price">FROM<span>$' . $price_range . '</span></div>
                                    <a class="btn btn-danger" href="details.php?ads_id='.$ads_id.'">LEARN MORE</a>
                                </div>
	</div>
</div>';

// echo $advertisements;
    }
}
?>
<html lang="en">

<?php include('header.php'); ?>

<body style="background: #f1f1f1">

<?php include('navigation.php'); ?>

<div class="content">
    <main role="main">

        <!-- Main jumbotron for a primary marketing message or call to action -->




        <div class="container m-t-10" >
            <!-- Example row of columns -->
            <div class="row">

                <?php echo $advertisements; ?>
            </div>

            <hr>

        </div> <!-- /container -->



    </main>

    <?php include('footer.php'); ?>

</div>


<!-- Bootstrap core JavaScript

================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../../../assets/js/vendor/popper.min.js"></script>
<script src="../../../../dist/js/bootstrap.min.js"></script> -->
</body>
</html>

