<?php
require "connect.inc.php";


?>
<html lang="en">

<?php include('header.php'); ?>

<body style="background: #f1f1f1">

<?php include('navigation.php'); ?>
<link rel="stylesheet" href="assets/booking/css/booking.css">


<div class="content">
    <main role="main">

        <!-- Main jumbotron for a primary marketing message or call to action -->


        <div class="text-center"
             style="background-image:url(assets/images/slide.jpg); background-size: cover;background-repeat: no-repeat; height:500px;margin-bottom:10px;width: 100%;opacity: 0.8">
        </div>


        <div class="container">
            <!-- Example row of columns -->
            <div class="row">

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading text-center">
                            <h4> Contact Info</h4>


                        </div>
                        <div class="panel-body">
                            <div class="map">
                                <p>TemplateMonster provides 24/7 support for all its <span class="col1"><a href="http://www.templatemonster.com/website-templates.php" rel="nofollow">premium products</a></span>. Freebies go without it.</p>
                                <p>If you have any questions regarding the customization of the chosen free theme, ask <span class="col1"><a href="http://www.templatetuning.com/" rel="nofollow">TemplateTuning</a></span> to help you on a paid basis.</p>
                                <div class="clear"></div>
                                <figure class="">
                                    <iframe src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Brooklyn,+New+York,+NY,+United+States&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=40.649974,-73.950005&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
                                </figure>
                                <address>
                                    <dl>
                                        <dt>The Company Name Inc. <br>
                                            8901 Marmora Road,<br>
                                            Glasgow, D04 89GR.
                                        </dt>

                                    </dl>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel">

                        <div class="panel-heading text-center">
                           <h4> Booking</h4>


                        </div>
                        <div class="panel-body">

                                <form class="row" action="booking.php" method="POST">
                                    <div class="col-sm-6 m-b-10">
                                        <input type="text" class="form-control" id="name" placeholder="Name" name="name">

                                    </div>
                                    <div class=" col-sm-6 m-b-10">

                                        <input type="email" class="form-control" id="email" name="email" required placeholder="Email">
                                    </div>
                                    <div class=" col-sm-6 m-b-10">

                                        <input type="text" class="form-control" id="country" placeholder="country">
                                    </div>
                                    <div class=" col-sm-6 m-b-10">

                                        <input type="text" class="form-control" id="Hotel" placeholder="Hotel">
                                    </div>
                                    <div class=" col-sm-12 m-b-10">

                                        <textarea rows="4"  class="form-control" id="Hotel" placeholder="Message" name="message" required></textarea>
                                    </div>
                                    <div class=" col-sm-12 m-b-10">

                                        <button class="btn btn-danger pull-right" type="submit"> Send</button>
                                    </div>


                                </form>
                            <form action="search.php" method="post">
                                <input type="text" class="form-control" name="search" placeholder="search for ads">
                                <button class="btn btn-success" type="submit">Search</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <hr>

        </div> <!-- /container -->


    </main>

    <?php include('footer.php'); ?>

</div>


<!-- Bootstrap core JavaScript

================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../../../assets/js/vendor/popper.min.js"></script>
<script src="../../../../dist/js/bootstrap.min.js"></script> -->
</body>
</html>

